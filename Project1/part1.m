% Project #1
%1.
costs = [];% An array to store all the computational costs.
precisions = [];% An array to store all the precision values.
differences = []; %An array to store all the diffences between mypi and true pi.
approximatedpi=[];
Ns = 20:20:300000;
for N = Ns % A fixed number of random points.
        n = 0; % the number of points that lies in the quarter of the circle
              %(successful points)
        tic % start the stopwatching timer
        for i = 1:N % for loop to let i goes from 1 to N, so that we can get N points;
            x(i) = rand(); % generate a random number in the interval (0,1)
            y(i) = rand(); % generate a random number in the interval (0,1)
            if (x(i))^2 + (y(i))^2 <= 1 % the distance between the point (x,y) and 
                %the origin is less or equal than 1, which means the point 
                %(x,y) is in the quarter.
                n = n + 1; % add 1 to the successful points.
            else 
            end
        end
        approximatedpi(end+1) = 4 * n/N; %compute the pi value.
        %store it at the end of the f-array.
        d = abs(approximatedpi(end) - pi);% the difference between the pi I compute with the real pi.
        time = toc; %end the stopwatching timer to get the exection time.
        p = precision(d); %call the precision function to get the precision
                          %with the the approximation of pi under N points
        
        differences = [differences, d]; %add the each difference into the array
        costs = [costs, time]; %add each executional time into the costs array.
        precisions = [precisions, p]; %add each precision value into this array
end
%plot the figure
figure(1)
plot(Ns,approximatedpi,Ns,differences) % plot the approximation pi value and difference value over the 
%different number of fixed points
xlabel('the numebr of fixed points')
ylabel('approximation pi value and the difference value')
legend('approximatedpi','differences')
figure(2)
scatter(costs,precisions)
xlabel("time of execution")
ylabel("precision")%plot the figure 


function p = precision(d)%define a function to calculate the precision of approximation
   if (d>=0.1)&& (d<1)    % set the judgement of precision level 1 (0.1<=d<1)
       p=1;
   else if (d>=0.01)&& (d<0.1)  % set the judgement of precision level 2 (0.01<=d<0.1)
     p=2;
       else if (d>=0.001)&& (d<0.01)  % set the judgement of precision level 3 (0.001<=d<0.01)
               p=3;
           else if (d>=0.0001)&& (d<0.001)  % set the judgement of precision level 4 (0.0001<=d<0.001)
               p=4;
               else 
                   p=5;                   % set the judgement of precision level 5 (d<0.0001)
               end
           end
       end
   end
end





      
       