%part3
%define the whole program as a function, and set mypi, which is the
%final computed value of pi, as the returned value.
function mypi = thirdpart()
%a
%for the user-defined level of precisions, we need to change the given 'ps'
%to the input by the prompt function.
%b 
%plot the square and the quarter of the circle
figure(3)
%draw square
sx = [0,1,1,0,0]; %the x value of the square
sy = [0,0,1,1,0]; %the y value of the square
plot(sx,sy,'b');
hold on
%draw a quarter of the circle in the square
cx = 0;%the center at the coordinate x
cy = 0;%the center at the coordinate y
r = 1;%the radius is 1
th= 0:pi/50:pi/2;
xunit = r*cos(th) + cx;
yunit = r*sin(th) + cx;
plot(xunit,yunit,'r');
hold off

hold on
 N = 1; %the total number of experiment points which are in the unit square.
 n = 0; %the total number of successful points which are located in
        %the quarter of the circle.
 outcomes = [];% define a array to store the pi's value I compute.
 steps=[];
 p = 0;
 prompt = 'Define a level of precision: ';
 ps = input(prompt);
 if  ismember(ps,1:5)
     while p <= ps % when the significant figures of pi we get is less than the
        %the fixed level, the while-loop will be executed.
       x = rand();%generate a random number between 0 and 1 as the x value of the point.
       y = rand();%generate a random number betwwen 0 and 1 as the y value of the point.
       if x^2 + y^2 <= r^2 %check if the point(x,y) in the quarter of circle.
           n = n + 1;     % if true, add one to the successful points.
           plot(x,y,'c.') %plot points in the circle
       else
           plot(x,y,'m.')%plot point out of the circle
       end 
       mypi = 4*n/N; % calculate the pi value
       outcomes = [outcomes, mypi];% store the pi value in the array.
       p = SignificantFigures(outcomes);
       N = N + 1; % increase the total experiment points by 1.
    end
    steps(end+1)=N-1
    %to display the outcome in the command window
    if ps == 1
       mypi = fprintf(' %.0f\n',mypi)
    else
       digits(ps);
       mypi = double(vpa(mypi));
       disp(mypi)
    end
    
    %to display the outcome in the plot(figure 3)
    if ps == 1
        inplot = num2str(mypi,0);
        text(0.3,0.4,inplot,'Fontsize',20);
    else 
        inplot = num2str(mypi);
        text(0.3,0.4,inplot,'Fontsize',20);
    end
    hold off
 else
     disp('Error Input Precision Level')
 end
     
end

% the function to transfer the error to the significant figures. Since we
% know that the pi value we compute is flucturated around the true value of
% pi from the first part of the project. I use the largest differences
% between the last 20 values to be the error( volatile), then to compute
% the relative erro to compute the precisions.
function p = SignificantFigures(outcomes)
    Len = length(outcomes);
    if Len > 20
      difference = max(outcomes(end-20: end)) - mean(outcomes(end-20:end));% use the last 20 pi
     %to compute the relative error, assume the average of the last 20 pis
     %is the true value.
    
    else
       difference = 0.5; %we do not want this error
    end
   if (difference>=0.1)&& (difference<1)    % set the judgement of precision level 1 (0.1<=d<1)
       p=1;
   else if (difference>=0.01)&& (difference<0.1)  % set the judgement of precision level 2 (0.01<=d<0.1)
     p=2;
       else if (difference>=0.001)&& (difference<0.01)  % set the judgement of precision level 3 (0.001<=d<0.01)
               p=3;
           else if (difference>=0.0001)&& (difference<0.001)  % set the judgement of precision level 4 (0.0001<=d<0.001)
               p=4;
               else if (difference>=0.00001)&& (difference<0.0001)
                   p=5;                   % set the judgement of precision level 5 (0.00001<=d<0.0001)
                   else
                       p=6;               %set the end of the judgement 
                   end
               end
           end
       end
   end
end

 
