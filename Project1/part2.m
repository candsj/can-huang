% part2
% Given the specific level of precision (let's say 2 and 3 here), then we use a 
% while loop to find how many random points we need to compute our pi in that 
% level of precision.
 N = 1; %the total number of experiment points which are in the unit square.
 n = 0; %the total number of successful points which are located in
        %the quarter of the circle.
 outcomes = [];% define a array to store the pi's value I compute.
 steps=[];
for ps = 2:3 %define 2 fixed levels of precision from 2 to 3 to get the value of pi.
    p = 0;
    while p < ps % when the significant figures of pi we get is less than the
        %the fixed level, the while-loop will be executed.
       x = rand();%generate a random number between 0 and 1 as the x value of the point.
       y = rand();%generate a random number betwwen 0 and 1 as the y value of the point.
       if x^2 + y^2 <= 1 %check if the point(x,y) in the quarter of circle.
           n = n + 1;     % if true, add one to the successful points.
       end 
       mypi = 4*n/N; % calculate the pi value
       outcomes = [outcomes, mypi];% store the pi value in the array.
       p = SignificantFigures(outcomes); %call the precision function to get the precision
       N = N + 1; % increase the total experiment points by 1.
    end 
    steps(end+1)=N-1
end
% the function to transfer the error to the significant figures. Since we
% know that the pi value we compute is flucturated around the true value of
% pi from the first part of the project. I use the largest differences
% between the last 20 values to be the error( volatile), then to compute the precisions.
function p = SignificantFigures(outcomes)
    Len = length(outcomes);
    if Len > 20
      difference = max(outcomes(end-20: end)) - mean(outcomes(end-20:end));% use the last 20 pi
     %to compute the relative error, assume the average of the last 20 pis
     %is the true value.
    
    else
       difference = 0.5; %we do not want this error, so we set the difference to make sure the precision levle equal 1
                         %while the length is less or equal to 20
    end
   if (difference>=0.1)&& (difference<1)    % set the judgement of precision level 1 (0.1<=d<1)
       p=1;
   else if (difference>=0.01)&& (difference<0.1)  % set the judgement of precision level 2 (0.01<=d<0.1)
     p=2;
       else if (difference>=0.001)&& (difference<0.01)  % set the judgement of precision level 3 (0.001<=d<0.01)
               p=3;
           else if (difference>=0.0001)&& (difference<0.001)  % set the judgement of precision level 4 (0.0001<=d<0.001)
               p=4;
               else 
                   p=5;                   % set the judgement of precision level 5 (d<0.0001)
               end
           end
       end
   end
end


