%partc: Write a Matlab function that will return the row canonical form for
%any given row echelon matrix.
%Define a function first, the input is a given matirx, and the output is
%the rowcanonical form of the matrix.
function RowCanonical = Project1partc(A)
    form = Project1parta(A);
    %check if the input matrix is in the row echelon form.
    if form == "neither"
        error("The input matrix is NOT in echelon form") 
    end
    %Then do the back-substitution
    [m,n] = size(A);
    i = m;
    j = n;
    while i >= 1 && j>=1
        %first find the last non-zero row, define this row the i-th row
        if isempty(find(A(i,:)))
            i = i-1;
        end
        %covert the pivot entry to '1'
        for j = 1:n
            if A(i,j) ~= 0
                A(i,:) = A(i,:).*(1/(A(i,j)));
                break
            end
        end
        %use the pivot to zero out the correspondinng entry in all row
        %above it.
        for k = 1:i-1
            A(k,:) = A(k,:)- A(k,j).*A(i,:);
            A(k,find(abs(A(k,:))<0.000005))= 0;
        end
        i = i - 1;
        j = j - 1;
    end
    RowCanonical = A;
end