%part e, test code with random matrices of various sizes, plot the time of
%the number of row m and column n.
%define a function, and you do not need to input anything, the output is
%the time required to solve the system.
function time = Project1parte()
    time1 = [];%create a array to store the times for the case m = n
    time2 = [];%create a array to store the times for the case fixed m and increase n;
    time3 = [];%create a array to store the times for the case fixed n and increase m;
    %case 1: m=n;
    mm = [];
    for m = 5:50
        %generate a m*n random matrix A, the elements in it is the
        %random number from 0 to 20.
        A = round(20*rand(m,m));
        %generate a m*1 vector b, the elements in it is the random
        %number from 0 to 30.
        b = round(30*rand(m,1));
        %start the stopwatch timer
        tic
        try
            solution = Project1partd(A,b);
        catch
            
        end
        time = toc;%read the elapsed time from stopwatch
        time1(end+1) = time;   
        mm(end+1) = m;
    end
    figure(1)
    plot(mm, time1)
    xlabel('the number of rows,m, and columns,n')
    ylabel('time required to solve the system')
    title('the case of m = n')
    %case 2: fixed m = 10,with increasing n
    nn = [];
    for n = 5:50
        m = 10;
        %generated random matrix A
        A = round(20*rand(m,n));
        b = round(30*rand(m,1));
        %start the stopwatch timer
        tic
        try
            solution = Project1partd(A,b);
        catch
            
        end
        time = toc;
        time2(end+1) = time;
        nn(end+1) = n;
    end
    figure(2)
    plot(nn,time2, '-o')
    xlabel('the number of columns,n')
    ylabel('time required to solve the system')
    title('the case of m = 10, and n is increasing')
    %case 3:fixed n = 10, with increasing m
    mm = [];
    for m = 5:50
        n = 10;
        A = round(20*rand(m,n));
        b = round(30*rand(m,1));
        tic
        try
            solution = Project1partd(A,b);
        catch
            
        end
        time = toc;
        time3(end+1) = time;
        mm(end+1) = m;
    end
    figure(3)
    plot(mm,time3)
    xlabel('the number of rows,m')
    ylabel('time required to solve the system')
    title('the case of n = 10, and m is increasing')
    time = [time1,time2,time3];
end

