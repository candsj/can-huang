%2 partc
%Write a function that computes the LU decomposition of an input matrix.
%The function should return L,U and P.
%Define a function, the input is a given matrix, and the output is the
%matrices L, U and P.
function [L,U,P] = Project2partc(A)
    A0 = A;%store the initial input A
    [m,n]= size(A);
    %initialize two identity matrices M0 and P0,which the # of rows and 
    %columns are both equal to the # of the given matrix's row.
    P0 = eye(m);
    M0 = eye(m);
    P = eye(m); % get the row_exchange matrices from forward elimintaion
    M = eye(m); %get all transformation from forward elimination.
    i = 1;
    j = 1;
    %do the forward elimination for the given matrix A.
    while i < m && j <= n
        if isempty(find(A(:,j))) % step 4, check if all entris in column j are zero 
            j = j + 1;
            continue
        end
        %% find the pivot and exchange rows k & i.
        for k = i:m
            if A(k,j) ~= 0 %find the first pivot in j-th column
                B = A(i,:);
                A(i,:) = A(k,:);
                A(k,:) = B;
                C = P(i,:);%do the samething to the identity matrix
                P(i,:) = P(k,:);
                P(k,:) = C;
                P = P*P0; %the product of row_exchange matrices
                break
            end
        end
         %% for loop to replace R_k. Use the pivot to zero out the
        %corresponding entry in all row below it.
        for k = i:m
            if k == i
                continue
            end
            D = [A,M];
            D(k,:) = D(k,:) - ((D(k,j))/(D(i,j))).*(D(i,:)); 
            D(k,isnan(D(k,:))) = 0;
            D(k,find(abs(D(k,:))<0.000005))= 0;
            M = D(:,n+1:end);
            A = D(:,1:n);
            M = M*M0;%the product of all other matrices from FE.
        end
       i = i + 1;
       j = j + 1;
    end
    U = M*A0; %get the upper triangular matrix "U"
    L = Project2partb(M);%get the inverse of 'M' 
end