function output = Project1parta(A)
%the output is a string(echelon form, row canonical form, or neither)
%the input is a given matrix A
    [m,n] = size(A);
    J = [];     % J - save the position of first non-zero element of each rows; n+1 if all zero.
    for i = 1:m 
        allzero = 1;%Since there are two ways to jump out of the for loop of j, 
                    %the first way is "break", 
                    %and the second one is that all the elements in this row is
                    %zero, so it has to go through this row. Therefore, I defined
                    %the second one as this row is all zero.
        for j = 1:n     
            if A(i,j) ~= 0          % find the first non-zero element of this row.        
                J(end+1) = j;
                allzero = 0;
                break 
            end
        end
        if allzero == 1             % case that the row is all-zeros.
            J(end + 1) = n + 1;
        end
    end
    %% Check if array J is sorted. If it is, it is a echelon form, and we can check if it is a 
       %row canonical form; if it is not, it is "neither".
       %From the definition of the echelon form, the first non-zero elements in the ith row
       %should strictly to the right of the (i-1)th row (if this row is not a zero row)
       %if the rows are the all zero rows, they will be at the bottom of
       %the matrix.
       for i = 2:length(J)
           if J(i) > J(i-1)
               continue
           elseif J(i) == J(i-1) && J(i) == n + 1
               continue
           else
               output = "neither";
               return
           end
       end
       output = "echelon form";
       %Then check for the row canonical form.
       for i = 1:m
           if J(i) == n+1 %if the row is all zero in an echelon form, neglect it.
               continue
           elseif A(i,J(i)) ~= 1%check if the pivot of the i-th row is equal to 1
               return 
           end
           for k = 1:i-1  %check if every elements above each pivot is zero.
               if A(k,J(i))~=0
                   return
               end
           end
       end
       output = "row canonical form";
end