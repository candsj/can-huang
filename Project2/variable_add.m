function output = variable_add(X,Y, sign)
% X - string in the left side.
% Y - string in the right side.
    if sign == 1
        output = strcat(X, '+');
    else
        output = strcat(X, '-');
    end
    output = strcat(output, Y);
end


