%using solutions to (1), add a computation of the appropriate elementary
%matrices to both forwar elimination and back substitution.
%Since each of the operations that are applied to the given matrix A can
%also applied to the identity matrix, which # of row and column are equal to
%the # matrix A's row. Then, I add an identity matrix when doing the Forward
%elimination to form an "elementary" matrix of the forward elimination. (e.g
%C = [ A,I]), 'I' will be formed to the elementary matrix when A become the
%row canonical form. When doing the backward substitution, I also add an 
%identity matrix to form an 'elementary' matrix of the backward 
%substitution.
function [F_E_E, B_S_E] = Project2parta(A)
    %get the size of matrix A to get the corresponding identity matrix
    [m,n] = size(A);
    I = eye(m);
    C = [A,I];
    F_E = Project1partb(C);
    F_E_E = F_E(:,n+1:end);%elementary matrices to forward elimination
    F_E_A = F_E(:,1:n);%get the matrix A after the forward elimination
    D = [F_E_A,I];
    B_S = Project1partc(D);
    B_S_E = B_S(:,n+1:end);%elementary matrices to backward elimination  
end