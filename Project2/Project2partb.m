%2,partb
%write a function that will return the inverse of a given input matrix, or
%an appropriate erroe message.
%Define a function, the input are either a matrix variable passed to the 
%function or a text file containing a matrix, the output is the inverse of
%the given matrix. If the input is a text file, write the inverse also to a
%textfile called 'inverse.txt'.
function answer = Project2partb(A) 
  %check if the input is a text file, if it is, read it to a matrix
    if ischar(A)%check if the input is a character array since isfile argument
                   %can only take the input as a string.
        if isfile(A)
            A = dlmread(A);
            inverse = getinverse(A);
            answer = inverse;
            dlmwrite('inverse.txt',inverse)
        end
    elseif ismatrix(A)
        inverse = getinverse(A);
        answer = inverse;
    end
end
function inverse = getinverse(A)
    [m,n] = size(A);
    %check if the matrix is a square matrix, only square matrix can have
    %a inverse.
    if m ~=n
        error('the matrix does not have an inverse')
    end
    %then, check if the matrix is singular. If a matrix is singular, then,
    %it is not full rank.
    if rank(A) ~= m
        error('the matrix is singular')
    end
    %Each of the operations of the forward elimination and
    %back_substitution which are applied to the given matrix A,
    %can be applied to the identity matrix to form an "elementary" matrix,
    %and the elementary matrix is the inverse of the given matrix A 
    %after A become a Row_canonical form with full rank. Therefore I add an 
    %identity matrix, which # row and column are equal to the given matrix's
    %# row, and combine them to a new matrix C = [A,I], then do the operations. 
    I = eye(m);
    C = [A,I];
    F_E = Project1partb(C);% do the forward elimination
    B_S = Project1partc(F_E); % do the backward elimination
    inverse = B_S(:,n+1:end);
end