%partb :Write a Matlab function that will return a row echelon form for any given
%in put matirx.
%Define a funciton first. The input is any given matrix A, and the output
%is the row echelon form for it. 
function RowEchelon = Project1partb(A)
    [m,n]= size(A); % get the number of rows and columns for this given matrix
    %find the pivot in the given matirx, and do the step 2~4
    i = 1;
    j = 1;
    while i < m && j <= n
        if isempty(find(A(:,j))) % step 4, check if all entris in column j are zero 
            j = j + 1;
            continue
        end
        %% find the pivot and exchange rows k & i.
        for k = i:m
            if A(k,j) ~= 0 %find the first pivot in j-th column
                B = A(i,:);
                A(i,:) = A(k,:);
                A(k,:) = B;
                break
            end
        end
        %% for loop to replace R_k. Use the pivot to zero out the
        %corresponding entry in all row below it.
        for k = i:m
            if k == i
                continue
            end
            A(k,:) = A(k,:) - ((A(k,j))/(A(i,j))).*(A(i,:)); 
            A(k,isnan(A(k,:))) = 0;
            A(k,find(abs(A(k,:))<0.000005))= 0;
        end
       i = i + 1;
       j = j + 1;
    end
    RowEchelon = A;
end 
